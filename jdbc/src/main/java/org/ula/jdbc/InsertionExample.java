package org.ula.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class InsertionExample {
	  // JDBC driver name and database URL
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	   static final String DB_URL = "jdbc:mysql://localhost:3306/testJDBC";

	   //  Database credentials
	   static final String USER = "root";
	   static final String PASS = "LabSC02";
	   
	   public static void main(String[] args) {
	   Connection conn = null;
	   PreparedStatement stmt = null;
	   try{
	      //STEP 2: Register JDBC driver
	      Class.forName("com.mysql.jdbc.Driver");

	      //STEP 3: Open a connection
	      System.out.println("Connecting to database...");
	      conn = DriverManager.getConnection(DB_URL,USER,PASS);

	      //STEP 4: Execute a query
	      System.out.println("Creating statement...");
	     
	      String sql;
	      sql = "INSERT INTO Persons VALUES (?,?,?,?,?)";
	      stmt = conn.prepareStatement(sql);
	      stmt.setInt(1,3);
	      stmt.setString(2, "Perez");
	      stmt.setString(2, "Pedro");
	      stmt.setString(2, "Merida");
	      int result = stmt.executeUpdate();
	      System.out.println("result: " + result);
	      //STEP 5: Extract data from result set
	      stmt.close();
	      conn.close();
	   }catch(SQLException se){
	      //Handle errors for JDBC
	      se.printStackTrace();
	   }catch(Exception e){
	      //Handle errors for Class.forName
	      e.printStackTrace();
	   }finally{
	      //finally block used to close resources
	      try{
	         if(stmt!=null)
	            stmt.close();
	      }catch(SQLException se2){
	      }// nothing we can do
	      try{
	         if(conn!=null)
	            conn.close();
	      }catch(SQLException se){
	         se.printStackTrace();
	      }//end finally try
	   }//end try
	   System.out.println("Goodbye!");
	}//end main
}
